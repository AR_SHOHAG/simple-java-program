main.java

Type
Java
Size
5 KB (4,627 bytes)
Storage used
5 KB (4,627 bytes)
Location
Java_Lab
Owner
me
Modified
Jul 12, 2017 by me
Opened
6:44 AM by me
Created
Jul 12, 2017 with Google Drive Web
Add a description
Viewers can download
import java.util.Scanner;

public class insert {
    Scanner in = new Scanner(System.in);
    String semester_name;
    int total_student, total_class, total_attand;
    String student_name[] = new String[100];
    String student_id[] = new String[100];
    int[][] attend = new int[100][150];
    int attend_serial = 0;

    void create_sudent() {
        System.out.println("Semester name: ");
        semester_name = in.nextLine();

        System.out.println("Total student : ");
        total_student = in.nextInt();
        in.nextLine();

        System.out.println("Total class : ");
        total_class = in.nextInt();
        in.nextLine();

        for (int i = 0; i < total_student; i++) {
            System.out.print("Student name: ");
            student_name[i] = in.nextLine();
            System.out.print("Student id: ");
            student_id[i] = in.nextLine();
        }
    }

    void show_content() {
        for (int i = 0; i <= 50; i++) {
            System.out.print("-");
            if (i == 50) {
                System.out.println();
            }
        }
        for (int i = 0; i <= 30; i++) {
            if (i == 0) {
                System.out.print("Student Name");
            } else if (i == 5) {
                System.out.print("Student id");
            } else if (i == 20) {
                System.out.print("attendance");
            } else if (i == 30) {
                System.out.println();
            } else {
                System.out.print(" ");
            }
        }
    }

    void insert_attendence() {
        for (int i = 0; i < total_student; i++) {
            System.out.print(student_name[i]);
            System.out.print("                ");
            System.out.print(student_id[i]);
            System.out.print("                    ");
            attend[i][attend_serial] = in.nextInt();
            in.nextLine();
            System.out.println();
        }
        attend_serial++;
    }

    int count_attendence(int[] attendances) {

        int attendance_qt = 0;

        for(int i = 0; i< attend_serial; i++) {
            if(attendances[i] == 1) {
              attendance_qt++;
            }
        }
        return attendance_qt;
    }

    void show_a_student(int i) {
            System.out.print("Student Name: " + student_name[i]);
            System.out.println();
            System.out.print("Student ID  : " + student_id[i]);
            System.out.println();
            System.out.print("Attandance  : " + count_attendence(attend[i]));
            System.out.println();
            System.out.print("----");
            System.out.println();
	}

    void search_by_id() {
        System.out.print("Enter id: ");
        String temp = in.nextLine();
        Boolean found = false;

        for(int i = 0; i < total_student; i++) {
            if(temp.compareTo(student_id[i]) == 0) {
                found = true;
                System.out.print("----");
                System.out.println();
                show_a_student(i);
                break;
            }
        }

        if(found == false) {
            System.out.print("student not found");
        }

        System.out.println();
        System.out.println();
    }

    void show_all() {
        System.out.print("---- show all student ----");
        System.out.println();
        for(int i = 0; i < total_student; i++) {
          show_a_student(i);
        }
    }
}

public class main {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        insert insert_obj = new insert();
        insert_obj.create_sudent();

        while(true) {
            System.out.println("Press:");
            System.out.println("1. attendence");
            System.out.println("2. search by ID");
            System.out.println("3. show all");
            System.out.println("4. exit");
            String press = in.nextLine();

            if(press.compareTo("1") == 0) {//insert complete

                insert_obj.show_content();
                insert_obj.insert_attendence();

            } else if(press.compareTo("2") == 0) {//search by id

                insert_obj.search_by_id();
            } else if(press.compareTo("3") == 0) {//show all

                insert_obj.show_all();

            } else if(press.compareTo("4") == 0) {//exit
                break;
            } else {
                System.out.println("Insert error!");
            }
        }
    }
}